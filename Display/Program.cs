﻿using System;
using System.Diagnostics;

namespace Display
{
    public class Program
    {
        private const int _width = 224;
        private const int _height = 256;
        private const int _bitsPerPixel = 1;
        private static readonly double _ticksPerFrame = Stopwatch.Frequency / 60.0;

        private static byte[] _pixelData = new byte[(int)(_width * (1.0/8 * _bitsPerPixel) * _height)];
        private static Random _random = new Random();

        [STAThread]
        static void Main(string[] args)
        {
            var display = new DisplayWindow(_width, _height)
            {
                RenderWidth = _width,
                RenderHeight = _height,
                RenderBitsPerPixel = _bitsPerPixel
            };
            display.FormClosed += Display_FormClosed;
            display.RunInNewThread();
            LoadPixelDataIgnoringAlpha();

            var time = Stopwatch.StartNew();
            double lastFrame = 0;
            while (true)
            {
                var now = time.ElapsedTicks;
                double ticksSinceLastFrame1 = now - lastFrame;
                if (ticksSinceLastFrame1 >= _ticksPerFrame)
                {
                    lastFrame = now;
                    LoadPixelDataIgnoringAlpha();
                    display.PixelData = _pixelData;
                }
            }
        }

        private static void Display_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private static void LoadPixelDataIgnoringAlpha()
        {
            //LoadPixelDataWithSingleColor(255, 0, 0);
            _random.NextBytes(_pixelData);
        }

        private static void LoadPixelDataWithSingleColor(byte red, byte green, byte blue)
        {
            if (_bitsPerPixel != 32)
            {
                throw new InvalidOperationException("1 bit per pixel is not supported in LoadPixelDataWithSingleColor");
            }
            for (var index = 0; index < _pixelData.Length; index += 4)
            {
                _pixelData[index + 0] = red;
                _pixelData[index + 1] = green;
                _pixelData[index + 2] = blue;

            }
        }
    }
}
