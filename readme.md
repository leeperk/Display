*Display*

An emulated display that can be used when implementing an emulator.  You tell it the width, height, and bits-per-pixel, then feed it a byte array of pixel data every time you want the display updated.  It's up to you to regulate the frequency of updates.